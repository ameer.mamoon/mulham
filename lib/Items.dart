// ignore_for_file: file_names, prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';

class Item extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    return Container(
      width: double.infinity,
      height: h*0.3,
      margin: EdgeInsets.all(w*0.05),
      decoration: BoxDecoration(
        border: Border.all()
      ),
      child: Column(
        children: [
          Expanded(child: Image.asset('images/chair.jpg')),
          ListTile(
            title: Text('Chair'),
            subtitle: Text('599.9 \$'),
            trailing: Icon(Icons.visibility),
            leading: Icon(Icons.favorite),
          )
        ],
      ),
    );
  }
}

class ItemsList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: 10,
      itemBuilder: (context,i){
        return Item();
      },
    );
  }
}

