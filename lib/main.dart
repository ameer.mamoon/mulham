import 'package:flutter/material.dart';

import 'Screens.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.red,
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(primary: Colors.red,)
        ),
        appBarTheme: AppBarTheme(color: Colors.red,),
        scaffoldBackgroundColor: Color(0xFFEEEEEE)
      ),
      routes: {
        'SignIn':(context) => SignIn(),
        'SignUp':(context) => SignUp(),
        'Home': (context)  => Home(),
        'Search': (context) => Search(),
      },
      initialRoute: 'SignIn',
    );
  }
}
