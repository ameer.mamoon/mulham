// ignore_for_file: file_names, prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';

import 'Items.dart';

class SignIn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text('Sign In'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: h*0.25,
            ),
            Container(
              color: Color(0xFFAAAAAA),
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(8),
              child: TextField(
                decoration: InputDecoration(
                  hintText: 'Email',
                  border: InputBorder.none,
                ),
                keyboardType: TextInputType.emailAddress,
              ),
            ),
            Container(
              color: Color(0xFFAAAAAA),
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(8),
              child: TextField(
                decoration: InputDecoration(
                  hintText: 'Password',
                  border: InputBorder.none,
                ),
                obscureText: true,
                keyboardType: TextInputType.visiblePassword,
              ),


            ),
            ElevatedButton(
                onPressed: (){Navigator.pushReplacementNamed(context, 'Home');},
                child: Text('Confirm'),
            ),
            GestureDetector(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text('Sign Up',style: TextStyle(color: Colors.purple),),
                ),
              onTap: (){
                  Navigator.pushReplacementNamed(context, 'SignUp');
              },
            ),
          ],
        ),
      ),
    );
  }
}

class SignUp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Sign Up'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              color: Color(0xFFAAAAAA),
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(8),
              child: TextField(
                decoration: InputDecoration(
                  hintText: 'Email',
                  border: InputBorder.none,
                ),
                keyboardType: TextInputType.emailAddress,
              ),
            ),
            Container(
              color: Color(0xFFAAAAAA),
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(8),
              child: TextField(
                decoration: InputDecoration(
                  hintText: 'Full Name',
                  border: InputBorder.none,
                ),
                keyboardType: TextInputType.text,
              ),
            ),
            Container(
              color: Color(0xFFAAAAAA),
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(8),
              child: TextField(
                decoration: InputDecoration(
                  hintText: 'Password',
                  border: InputBorder.none,
                ),
                obscureText: true,
                keyboardType: TextInputType.visiblePassword,
              ),


            ),
            Container(
              color: Color(0xFFAAAAAA),
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(8),
              child: TextField(
                decoration: InputDecoration(
                  hintText: 'Confirm Password',
                  border: InputBorder.none,
                ),
                obscureText: true,
                keyboardType: TextInputType.visiblePassword,
              ),


            ),
            Container(
              color: Color(0xFFAAAAAA),
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(8),
              child: TextField(
                decoration: InputDecoration(
                  hintText: 'Phone',
                  border: InputBorder.none,
                ),
                keyboardType: TextInputType.phone,
              ),
            ),
            ElevatedButton(
                onPressed: (){Navigator.pushReplacementNamed(context, 'Home');},
                child: Text('Confirm')
            ),
            GestureDetector(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('Sign In',style: TextStyle(color: Colors.purple),),
              ),
              onTap: (){Navigator.pushReplacementNamed(context, 'SignIn');},
            ),
          ],
        ),
      ),
    );
  }
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Home'),
        actions: [
          IconButton(onPressed: (){Navigator.pushNamed(context, 'Search');}, icon: Icon(Icons.search)),
        ],
      ),
      body: ItemsList(),
      drawer: Drawer(),
    );
  }
}

class Search extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: TextField(
          decoration: InputDecoration(
            hintText: 'Search',
          ),
          style: TextStyle(
            color: Colors.white
          ),
        ),
        actions: [
          IconButton(onPressed: (){}, icon: Icon(Icons.search)),
        ],
      ),
      body: Center(child: Text('Search'),),
    );
  }
}
